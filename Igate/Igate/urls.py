#models
from django.db import models

class Person(models.Model):
    name=models.CharField(max_length=100)
    phone=models.CharField(max_length=15,unique=True)
    password=models.CharField(max_length=100)
    def __str__(self):
        return self.name
class Picture(models.Model):
    picowner=models.ForeignKey(Person,on_delete=models.CASCADE)
    image=models.ImageField(upload_to='media')
    
#serializer
from rest_framework.serializers import ModelSerializer
class Personserializer(ModelSerializer):
    class Meta:
        model=Person
        fields='__all__'
        extra_kwargs={'password':{"write_only":True}}
        
class pictureserializer(ModelSerializer):
    class Meta:
        model=Picture
        fields='__all__'

#backend views
from rest_framework.decorators import APIView
from rest_framework.response import Response

class PersonView(APIView):
    #get  users(one or many)
    def get(self,request):
        try:
            id=request.data['id']
            people=Person.objects.get(id=id).first()
            seripeople=Personserializer(people)
            return Response(seripeople.data)
        except:
            #admin only
            people=Person.objects.all()
            seripeople=Personserializer(people,many=True)
            return Response(seripeople.data)
    #add a user
    def post(self,request):
        data=request.data
        seripeople=Personserializer(data=data)
        if seripeople.is_valid():
            seripeople.save()
            return Response("saved")
        else:
            return Response("not saved")
    #update a user
    def put(self,request):
        pass
    
    def delete(self,request):
        pass
    
    
from rest_framework.generics import CreateAPIView
class createpics(CreateAPIView):
    queryset=Picture.objects.all()
    serializer_class=pictureserializer
    
class pictureview(APIView):
    def get(self,request):
        pics=Picture.objects.all()
        picseri=pictureserializer(pics,many=True)
        return Response(picseri.data)
    def delete(self,request):
        pass
    

#frontend Views
from django.shortcuts import render,redirect
def Homepage(request):
    return render(request,'app/homepage.html')
def login(request):
    return redirect(redirect,'logons/login.html')
def register(request):
    return redirect(redirect,'logons/register.html')
def fogot(request):
    return redirect(redirect,'logons/fogot.html')


from django.contrib import admin
from django.urls import path


rpi=[]

backend=[
    path('api/person',PersonView.as_view(),name='personview'),#managing users(login,register,delete,e.t.c)
    path('api/pic/create',createpics.as_view(),name='createpics'),#creating pictures
    path('api/pic/fetch',pictureview.as_view(),name='allpics')
]
frontend=[
    path('home',Homepage,name='homepage'),
    path('login',login,name='login'),
    path('register',register,name='register'),
    path('fogot',fogot,name='fogot')
]


urlpatterns=backend+frontend+rpi